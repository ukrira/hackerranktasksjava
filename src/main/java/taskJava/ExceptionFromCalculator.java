package taskJava;

import java.util.Scanner;

//https://www.hackerrank.com/challenges/java-exception-handling/problem?isFullScreen=true
class MyCalculator {
    public int power(int n, int p) throws Exception {
        int result = n;
        if (n < 0 || p < 0) {
            throw new Exception("n or p should not be negative.");
        } else if (n == 0 & p == 0) {
            throw new Exception("n and p should not be zero.");
        } else if (p == 0) {
            result = 1;
        } else {
            while (p > 1) {
                result *= n;
                p--;
            }
        }
        return result;
    }

}

public class ExceptionFromCalculator {
    public static final MyCalculator my_calculator = new MyCalculator();
    public static final Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        while (in.hasNext()) {
            int n = in.nextInt();
            int p = in.nextInt();
            try {
                System.out.println(my_calculator.power(n, p));
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }
}
