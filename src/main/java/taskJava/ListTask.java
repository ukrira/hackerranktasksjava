package taskJava;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ListTask {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int initNumber = scanner.nextInt();
        List<Integer> integerList = new ArrayList<>(initNumber);
        for(int i = 0; i< initNumber; i++){
            integerList.add(scanner.nextInt());
        }
        int amountOfQueries =  scanner.nextInt();
        for (int k = 0; k<= amountOfQueries * 2 ; k++){
            String kindQuery = scanner.nextLine();
            if (kindQuery.equals("Insert")){
                int insertIndex = scanner.nextInt();
                int number = scanner.nextInt();
                integerList.add(insertIndex, number);
            }if(kindQuery.equals("Delete")){
                int deleteIndex = scanner.nextInt();
                integerList.remove(deleteIndex);
                System.out.println("delete");
            }
        }
        scanner.close();
      for(int num:integerList){
          System.out.print(num + " ");
      }

    }
}
