package taskJava;

import java.util.Stack;


public class Brackets {
    static String isBalanced(String s) {
        Stack<Character> openBracketsList = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '{' || s.charAt(i) == '[' || s.charAt(i) == '(') {
                openBracketsList.push(s.charAt(i));
            } else {
                if (openBracketsList.isEmpty()) {
                    return "NO";
                } else {
                    char last = openBracketsList.pop();
                    if (s.charAt(i) == '}' && last != '{') {
                        return "NO";
                    } else if (s.charAt(i) == ']' && last != '[') {
                        return "NO";
                    } else if (s.charAt(i) == ')' && last != '(') {
                        return "NO";
                    }
                }
            }
        }
        if (openBracketsList.isEmpty()) {
            return "YES";
        } else {
            return "NO";
        }
    }


    public static void main(String[] args) {
        System.out.println(isBalanced("{}(){}}"));
    }
}
