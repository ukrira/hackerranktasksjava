package taskJava;
//https://www.hackerrank.com/challenges/java-biginteger/problem?isFullScreen=true
import java.math.BigInteger;
import java.util.Scanner;

public class BigintegerAddMultiply {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String firstNumber = in.nextLine();
        String secondNumber = in.nextLine();
        in.close();
        BigInteger firstBI= new BigInteger(firstNumber);
        BigInteger secondBI = new BigInteger(secondNumber);
        System.out.println(firstBI.add(secondBI));
        System.out.println(firstBI.multiply(secondBI));
    }
}
