package taskJava;

import java.util.Scanner;

//IP address is a string in the form "A.B.C.D",
// where the value of A, B, C, and D may range from 0 to 255.
// Leading zeros are allowed. The length of A, B, C, or D can't be greater than 3.

//https://www.hackerrank.com/challenges/java-regex/problem?isFullScreen=true

public class Regex {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String IP = in.next();
            System.out.println(IP.matches(new MyRegex().pattern));
        }
    }

    static class MyRegex {
        String ip0to255 = "(\\d{1,2}||(0|1)\\d{2}||2[0-4]\\d||25[0-5])";
        //    \d represents digits in regular expressions, same as [0-9]
        //    \\d{1, 2} catches any one or two-digit number
        //    (0|1)\\d{2} catches any three-digit number starting with 0 or 1.
        //    2[0-4]\\d catches numbers between 200 and 249.
        //    25[0-5] catches numbers between 250 and 255.
        public String pattern = ip0to255 + "\\." + ip0to255 + "\\." + ip0to255 + "\\." + ip0to255;
    }
}
