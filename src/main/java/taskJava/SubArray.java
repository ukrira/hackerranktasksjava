package taskJava;
//https://www.hackerrank.com/challenges/java-negative-subarray/problem?isFullScreen=true
import java.util.Arrays;
import java.util.Scanner;

public class SubArray {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] a = new int[n];
        for (int k = 0; k < n; k++) {
            a[k] = scan.nextInt();
        }
        scan.close();

        int negativeAmount = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = i ; j < a.length; j++) {
                System.out.print(Arrays.toString(Arrays.copyOfRange(a, i, j + 1)));
                int[] subarray = Arrays.copyOfRange(a, i, j + 1);
                System.out.println(" sum "+arraySum(subarray));
                if (arraySum(subarray) < 0) {
                    negativeAmount++;
                }
            }
        }
        System.out.println(negativeAmount);
    }

    public static int arraySum(int[] array) {
        int sum = 0;
        for (int number : array) {
            sum += number;
        }
        return sum;
    }
}
