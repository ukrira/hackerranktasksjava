package taskJava;
// https://www.hackerrank.com/challenges/java-sort/problem?isFullScreen=true

import java.util.*;

class Student {
    private int id;
    private String fname;
    private double cgpa;

    public Student(int id, String fname, double cgpa) {
        super();
        this.id = id;
        this.fname = fname;
        this.cgpa = cgpa;
    }

    public int getId() {
        return id;
    }

    public String getFname() {
        return fname;
    }

    public double getCgpa() {
        return cgpa;
    }
}

class StudentComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        double compareToCgpa = o2.getCgpa() - o1.getCgpa();
        if (compareToCgpa > 0) {
            return 1;
        } else if (compareToCgpa < 0) {
            return -1;
        } else {
            int compareToFName = o1.getFname().compareTo(o2.getFname());
            if (compareToFName > 0) {
                return 1;
            } else if (compareToFName < 0) {
                return -1;
            } else {
                return (o1.getId() - o2.getId());
            }
        }
    }
}


public class SortStudent {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int testCases = Integer.parseInt(in.nextLine());

        List<Student> studentList = new ArrayList<>();
        while (testCases > 0) {
            int id = in.nextInt();
            String fname = in.next();
            double cgpa = in.nextDouble();
            Student st = new Student(id, fname, cgpa);
            studentList.add(st);
            testCases--;
        }
        StudentComparator comparatorSortStudent = new StudentComparator();
        studentList.sort(comparatorSortStudent);
        for (Student st : studentList) {
            System.out.println(st.getFname());
        }
// the second way of comparison
        Comparator<Student> lambdaComparator = Comparator
                .comparing(Student::getCgpa)
                .reversed()
                .thenComparing(Student::getFname)
                .thenComparing(Student::getId);
        studentList.sort(lambdaComparator);
    }
}
