package taskJava;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Java2DArray {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        List<List<Integer>> arr = new ArrayList<>();
        IntStream.range(0, 6).forEach(i -> {
            try {
                arr.add(
                        Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                                .map(Integer::parseInt)
                                .collect(toList())
                );
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }

        });
        bufferedReader.close();
        Integer firstSum = getSum(arr, 0, 0);

        Integer sumMax = firstSum;
        for (int line = 0; line < 4; line++) {
            for (int cell = 0; cell < 4; cell++) {
                Integer sum = getSum(arr, line, cell);
                if (sumMax < sum) {
                    sumMax = sum;
                }
            }
        }
        System.out.println(sumMax);
    }

    private static Integer getSum(List<List<Integer>> arr, int line, int cell) {
        Integer sum = arr.get(line).get(cell);
        sum += arr.get(line).get(cell + 1);
        sum += arr.get(line).get(cell + 2);
        sum += arr.get(line + 1).get(cell + 1);
        sum += arr.get(line + 2).get(cell);
        sum += arr.get(line + 2).get(cell + 1);
        sum += arr.get(line + 2).get(cell + 2);
        return sum;
    }
}
