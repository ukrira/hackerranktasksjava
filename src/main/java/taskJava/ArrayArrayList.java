package taskJava;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//https://www.hackerrank.com/challenges/java-arraylist/problem?isFullScreen=true
public class ArrayArrayList {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<ArrayList<Integer>> outerList = new ArrayList<>();
        int amountOfLines = scanner.nextInt();
        for (int i = 0; i < amountOfLines; i++) {
            int amountOfNumbersInLine = scanner.nextInt();
            ArrayList<Integer> innerList = new ArrayList<>();
            for (int j = 0; j < amountOfNumbersInLine; j++) {
                innerList.add(scanner.nextInt());
            }
            outerList.add(i, innerList);
        }
        int amountOfQueries = scanner.nextInt();
        for (int q = 0; q < amountOfQueries; q++) {
            int numberOfLines = scanner.nextInt() - 1;
            int numberOfCell = scanner.nextInt() - 1;
            if ((numberOfLines + 1 > amountOfLines)
                    || (outerList.get(numberOfLines).size() < numberOfCell + 1)) {
                System.out.println("ERROR!");
            } else System.out.println(outerList.get(numberOfLines).get(numberOfCell));
        }
        scanner.close();
    }
}
