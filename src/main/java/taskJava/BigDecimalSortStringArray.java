package taskJava;
//https://www.hackerrank.com/challenges/java-bigdecimal/problem?isFullScreen=true
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class BigDecimalSortStringArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        // I guess, this array was given in size n+2 for mistake
        String[] s = new String[n + 2];
        for (int i = 0; i < n; i++) {
            s[i] = sc.next();
        }
        sc.close();
        s = Arrays.copyOfRange(s, 0, n);

        Comparator<String> myStringDecimalComparator = (o1, o2) -> {
            BigDecimal bd1 = new BigDecimal(o1);
            BigDecimal bd2 = new BigDecimal(o2);
            return bd2.compareTo(bd1);
        };
        Arrays.sort(s, myStringDecimalComparator);
        Arrays.stream(s).forEach(System.out::println);

//        TreeMap<BigDecimal, String> treeMap = new TreeMap<>((o1, o2) -> o2.compareTo(o1));
//        for (int i = 0; i < s.length-2; i++) {
//            treeMap.put(new BigDecimal(s[i]), s[i]);
//        }
//        treeMap.forEach((key, value) -> System.out.println(value));
    }
}
