package taskJava;

import java.util.InputMismatchException;
import java.util.Scanner;

//https://www.hackerrank.com/challenges/java-exception-handling-try-catch/problem?utm_campaign=challenge-recommendation&utm_medium=email&utm_source=24-hour-campaign
public class ExceptionHandler {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        try {
           int a = scan.nextInt();
           int b = scan.nextInt();
            System.out.println(a/b);
        } catch (InputMismatchException ex) {
            System.out.println("InputMismatchException ex");
        } catch (ArithmeticException ex){
            System.out.println("ArithmeticException ex / by zero");
        }finally {
            scan.close();
        }
    }
}
